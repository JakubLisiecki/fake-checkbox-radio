document.querySelectorAll('label.fake-checkbox-radio-container').forEach( inputContainer => {
    const checkbox = inputContainer.querySelector('input[type=checkbox]');
    const fakeCheckbox = inputContainer.querySelector('.checkbox');

    if(checkbox && fakeCheckbox) {
        if(checkbox.checked) fakeCheckbox.classList.add('checked');
        checkbox.addEventListener('change', event => {
            if(checkbox.checked) fakeCheckbox.classList.add('checked');
            else fakeCheckbox.classList.remove('checked');
        });
        return;
    }

    const fakeRadio = inputContainer.querySelector('.radio');
    const radio = inputContainer.querySelector('input[type=radio]');
    const radioContainers = Array.from(document.querySelectorAll(`label.fake-checkbox-radio-container input[type=radio][name=${radio.name}]`)).map( radio => radio.parentNode );

    if(radio && fakeRadio) {
        if(radio.checked) fakeRadio.classList.add('checked');
        radioContainers.forEach( container => {
            container.querySelector('input[type=radio]').addEventListener('change', () => {
                radioContainers.forEach( rC => {

                    const r = rC.querySelector('input[type=radio]');
                    const fR = rC.querySelector('.radio');

                    if(r.checked) fR.classList.add('checked');
                    else fR.classList.remove('checked');
                })
            })
        })
        return;
    }

});